import { Component } from '@angular/core';
// Un componente en angular son piezas de codigo que van a formar
// nuestra aplicacion. Son parecidos a lo que son los controladores
// en Spring
@Component({
  selector: 'app-root', // Este selector se muestra en el index.html
  // El lugar donde utilizaremos esta etiqueta es donde se mostrara
  // el contenido que fijemos en este archivo
  templateUrl: './app.component.html', // Asigna la vista
  styleUrls: ['./app.component.css'] // Asigna la hoja de estilos
  // Si generamos estilos globales los colocamos dentro del archivo
  // style.css
})
export class AppComponent {
  // Definimos los atributos para la clase AppComponent, que luego
  // llamaremos en app.component.html escribiendo, por ejemplo, 
  // Curso: {{ curso }}
  title = 'Bienvenido a Angular';
  curso: string = "Angular con Spring 5";
  profesor: string = "Andrés Guzmán"
  alumno: string = "José María"
}
