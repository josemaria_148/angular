import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { of, Observable } from 'rxjs';
// Para conectar con el servidor remoto
import { HttpClient } from '@angular/common/http';
// Para convertir el response del urlEndPoint en un objeto
// Cliente[] con formato json
import { map } from 'rxjs/operators';

// @Injectable representa la logica de negocio. Es lo que
// diferencia a una clase component de un servicio. Se puede
// inyectar a otros componentes
@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private urlEndPoint: string = 'http://localhost:8080/api/clientes';
  constructor(private http: HttpClient) { }

  // Esta funcion (sincrona) no permite las llamadas asincronas, por lo que no 
  // podria funcionar en un contexto real con una api rest, donde se
  // pueden realizar varias llamadas simultaneas que prevalezcan en el
  // lado del servidor
  // getClientes(): Cliente[] {
  //   return CLIENTES;
  // }

  // Convertimos / Creamos nuestro flujo observable a partir de los
  // objetos clientes y, por lo tanto, en un stream, es decir, en un
  // flujo de datos, lo cual es de vital importancia para que puedan
  // realizarse llamadas asincronas del lado del servidor
  // getClientes(): Observable<Cliente[]> {
  //   return of(CLIENTES);
  // }

  // Recuperamos los datos del servidor remoto de la siguiente manera
  getClientes(): Observable<Cliente[]> {
    // Casteamos el resultado a un arreglo de tipo Cliente
    // Esto va a retornar un observable, en este caso, un objeto de tipo json
    // return this.http.get<Cliente[]>(this.urlEndPoint);

    // Otra forma de hacerlo seria teniendo en cuenta el import map
    return this.http.get(this.urlEndPoint).pipe(
      // Casteamos el response con formato json de tipo generico a un arreglo
      // de cliente[]
      map( response => response as Cliente[] )
      // En ECMAScript6 se escribiria
      // map( function(response) { return response as Cliente[] } )
    );
  }

}
