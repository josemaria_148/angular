import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];

  // Pasamos como parametro un atributo de tipo ClienteService
  constructor(private clienteService: ClienteService) { }

  // Funcion que se ejecuta cuando se inicia el componente
  ngOnInit(): void {
    // Esto serviria si declaramos la funcion sincrona getClientes()
    // en cliente.service.ts
    //this.clientes = this.clienteService.getClientes();

    // Para realizarlo de forma asincrona
    this.clienteService.getClientes().subscribe(
      // La idea es asignar en el atributo clientes de clientes.component.ts
      // el valor que se está recibiendo de clienteService, que seria el listado
      // de clientes. Este seria nuestro observador, que actualiza el listado de
      // clientes que se pasa a la plantilla con los posibles cambios
      clientes => this.clientes = clientes
      
      // Esto mismo puede escribirse como
      // function (clientes) {
      //   this.clientes = clientes;
      // }
    );
  }

}
