import { Cliente } from "./cliente";

export const CLIENTES: Cliente[] = [
    {id: 1, nombre: 'Pepe', apellido: 'Blázquez', email: 'josemaria@gmail.com', createAt: '2019-04-18'},
    {id: 2, nombre: 'Luis', apellido: 'Blázquez', email: 'luis@gmail.com', createAt: '2019-04-18'},
    {id: 3, nombre: 'María', apellido: 'Pinilla', email: 'maria@gmail.com', createAt: '2019-04-18'},
    {id: 4, nombre: 'Pedro', apellido: 'Pinilla', email: 'pedro@gmail.com', createAt: '2019-04-18'},
    {id: 5, nombre: 'Juan', apellido: 'Pinilla', email: 'juan@gmail.com', createAt: '2019-04-18'},
    {id: 6, nombre: 'Elena', apellido: 'Travado', email: 'elena@gmail.com', createAt: '2019-04-18'},
    {id: 7, nombre: 'Lara', apellido: 'Fernández', email: 'lara@gmail.com', createAt: '2019-04-18'},
    {id: 8, nombre: 'Ignacio', apellido: 'Sánchez', email: 'ignacio@gmail.com', createAt: '2019-04-18'},
    {id: 9, nombre: 'Fran', apellido: 'Cuesta', email: 'fran@gmail.com', createAt: '2019-04-18'},
    {id: 10, nombre: 'Sergio', apellido: 'Iglesias', email: 'sergio@gmail.com', createAt: '2019-04-18'},
    {id: 11, nombre: 'Jorge', apellido: 'Vicente', email: 'jorge@gmail.com', createAt: '2019-04-18'}   
  ];