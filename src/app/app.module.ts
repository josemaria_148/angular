import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
// Importamos la clase header.component.ts
// Entre llaves escribimos el nombre de la clase en cuestion
// Despues registramos el nombre de la clase en Declarations
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
// Para mapear nuestros components a una cierta url
import { RouterModule, Routes } from '@angular/router';
// Esto permite la conexion entre la clase service y el servidor.
// El http es el componente que tiene angular para la comunicacion
// con el servidor remosto
import { HttpClientModule } from '@angular/common/http';

// Contante que contiene un arreglo con las rutas y que debemos incluir
// en los imports
const routes: Routes = [
  {path: '', redirectTo: '/clientes', pathMatch: 'full'},
  {path: 'directivas', component: DirectivaComponent},
  {path: 'clientes', component: ClientesComponent},
];

// Archivo donde se registran nuestros componentes, modulos, clases
// de servicio, muy parecido a lo que seria un contenedor en Spring
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  // En providers incluimos las clases service que se pueden inyectar en otros
  // componentes de la aplicacion
  providers: [ClienteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
