import { Component } from '@angular/core';

// Notacion propia de la clase component
// Para registrar esta clase tenemos que ir al archivo app.module.ts
// Una vez configurada la clase en app.module.ts, incluimos el selector
// por medio de una etiqueta en el archivo app.component.html
@Component({
    selector: 'app-header',
    // Si escribimos el html en este archivo utilizamos "template: ",
    // si no, tenemos que escribir "templateUrl: " para hacer referencia
    // a la ruta
    templateUrl: './header.component.html'
})
// Definimos la firma de la clase con export para que luego podamos
// importar la clase en el archivo app.module.ts
export class HeaderComponent {
    // Para inyectar title en el template escribimos {{ title }}
    title: string = 'App Angular';
}