import { Component } from '@angular/core';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    // Definimos la hoja de estilos (puede haber varias)
    styleUrls: ['./footer.component.css']
})

// Definimos la firma de la clase con export para que luego podamos
// importar la clase en el archivo app.module.ts
export class FooterComponent {
    // El tipo "any" es generico, es decir, puede ser de cualquier tipo
    // Para invocar los atributos "nombre" y "apellido" en el html
    // escribimos autor.nombre y autor.apellido
    public autor: any = {nombre: 'Andrés', apellido: 'Guzmán'};
}
